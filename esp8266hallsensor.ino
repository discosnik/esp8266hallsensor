#include<ESP8266WiFi.h>
#include<ESP8266HTTPClient.h>
#include<ArduinoJson.h>

//######PINS and Variables######
const byte interruptPin = 13; // D7
volatile byte interruptCounter = 0;

//######IP Settings######
boolean staticIPConfig = false;

//IP Config staticIPConfig = true, below settings are used
IPAddress ip(192, 168, 1, 17);

IPAddress subnet(255, 255, 255, 0);

IPAddress gateway(192, 168, 1, 1);

//######WiFi Settings######
const char* ssid = "networkname";
const char* password = "password";

//######HTTP application/x-www-form-urlencoded Settings######
boolean urlencoded = true;
String endpoint = "http://192.168.1.106/sensorvalueget";
String contentTypeDeclaration = "Content-Type";
String contentType = "text/plain";
String httpPost = "?sensorValue=1";
//######HTTP application/json Settings######
boolean json = true;
String contentTypeJSON = "application/json";
String endpointJSON = "http://192.168.1.106/hallsensor/";

//######DEBUG Settings######
boolean debug = true;

void setup() {
	Serial.begin(115200);
	if (debug == true) {
		Serial.println("setting up!");
	}
	connectWiFi();
	pinMode(interruptPin, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt,
			FALLING);
	if (debug == true) {
		Serial.println("checking self");
	}
	selfCheck();
}

void handleInterrupt() {
	detachInterrupt(digitalPinToInterrupt(interruptPin));
	sendWebRequest();
	attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt,
			FALLING);
}

void loop() {
	if (WiFi.status() != WL_CONNECTED) {
		Serial.println("WIFI DISCONNECTED!");
	}
}

void sendWebRequest() {
	if (WiFi.status() == WL_CONNECTED) {

		if (urlencoded) {
			sendUrlEncoded();
		}
		if (json) {
			sendJSON();
		}
	} else {
		WiFi.disconnect();
		connectWiFi();
		sendWebRequest();
	}

}

void connectWiFi() {
	if (debug == true) {
		Serial.println("start WiFi connect!");
	}
	if (staticIPConfig) {
		WiFi.config(ip, gateway, subnet);
	}
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED) {
		delay(2000);
		if (debug == true) {
			Serial.print("Connecting..");
		}
	}
}

void sendUrlEncoded() {
	HTTPClient http;
	http.begin(endpoint + httpPost);
	http.addHeader(contentTypeDeclaration, contentType);
	int httpCode = http.GET();
	if (debug == true) {
		Serial.println("Sending to " + endpoint + httpPost);
		if (httpCode > 0) {
			String payload = http.getString();
			Serial.println("http Code " + String(httpCode, DEC));
			Serial.println("payload " + payload);
		} else {
			Serial.println(
					"ERROR SENDING WITH MESSAGE:  " + getErrorCode(httpCode));
		}
	}
	http.end();
}

void sendJSON() {
	HTTPClient http;
	StaticJsonBuffer < 300 > JSONbuffer;
	JsonObject& JSONencoder = JSONbuffer.createObject();
	JSONencoder["sensorValue"] = "1";
	char JSONmessageBuffer[300];
	JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
	if (debug == true) {
		Serial.println("Sending to " + endpointJSON);
		Serial.println(JSONmessageBuffer);
	}
	http.begin(endpointJSON);
	http.addHeader(contentTypeDeclaration, contentTypeJSON);
	int httpCode = http.POST(JSONmessageBuffer);

	if (debug == true) {
		if (httpCode > 0) {
			String payload = http.getString();
			Serial.println("http Code " + String(httpCode, DEC));
			Serial.println("payload " + payload);
		} else {
			Serial.println(
					"ERROR SENDING WITH MESSAGE:  " + getErrorCode(httpCode));
		}
	}
	http.end();
}

void selfCheck() {
	pinMode(A0, INPUT);
	double raw = analogRead(A0);
	double volt = raw / 1023.0;
	volt = volt * 3.3;
	if (WiFi.status() == WL_CONNECTED && volt > 0.03) {
		Serial.print("Wifi Connected with IP:");
		Serial.print(WiFi.localIP());
		Serial.println(
				"! Hall Sensor Connected, actual reading: " + String(volt, DEC)
						+ " V ");
		Serial.println("The System is READY!");
	} else if (volt < 0.03) {
		Serial.print("Hall Sensor NOT Connected");
		Serial.println("SYSTEM MALFUNCTION!!");

	}
}

String getErrorCode(int httpError) {
	switch (httpError) {
	case -1:
		return "HTTPC_ERROR_CONNECTION_REFUSED";
		break;
	case -2:
		return "HTTPC_ERROR_SEND_HEADER_FAILED";
		break;
	case -3:
		return "HTTPC_ERROR_SEND_PAYLOAD_FAILED";
		break;
	case -4:
		return "HTTPC_ERROR_NOT_CONNECTED";
		break;
	case -5:
		return "HTTPC_ERROR_CONNECTION_LOST";
		break;
	case -6:
		return "HTTPC_ERROR_NO_STREAM";
		break;
	case -7:
		return "HTTPC_ERROR_NO_HTTP_SERVER";
		break;
	case -8:
		return "HTTPC_ERROR_TOO_LESS_RAM";
		break;
	case -9:
		return "HTTPC_ERROR_ENCODING";
		break;
	case -10:
		return "HTTPC_ERROR_STREAM_WRITE";
		break;
	case -11:
		return "HTTPC_ERROR_READ_TIMEOUT";
		break;

	default:
		return "no error";
		break;
	}

}
